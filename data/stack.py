"""Contains the class and related variables for the stack data structure."""

from typing import Generic, Optional, TypeVar

T = TypeVar('T')


class StackUnderflowError(Exception):
    """When items are requested from the stack, but it doesn't contain enough elements to return."""
    pass


class Stack(Generic[T]):
    """A stack (LIFO) data structure implementation."""

    def __init__(self, size: int):
        """Initialises the internal list and size.

        Args:
            size (int): The maximum number of elements the stack can hold.
        """
        self._list = []
        self._size = size

    def __str__(self):
        return '\n'.join(str(x) for x in self._list)

    def __len__(self):
        return len(self._list)

    def can_push(self) -> bool:
        """Checks whether the stack is full.

        Returns:
            bool: True if not full; False if no more items can be added without popping.
        """
        return len(self) < self._size

    def push(self, obj: T) -> bool:
        """Adds an object to the top of the stack.

        Args:
            obj (T): The object to add to the stack.

        Returns:
            bool: True if it was successful; False if the stack would overflow.
        """
        if self.can_push():
            self._list.append(obj)
            return True
        else:
            return False

    def pop(self) -> Optional[T]:
        """Returns the item at the top of the stack, removing it from the stack.

        Returns:
            Optional[T]: Either the item at the top of the stack, or None if it is empty.
        """
        if len(self) > 0:
            return self._list.pop()
        else:
            return None

    def peek(self) -> Optional[T]:
        """Returns the item at the top of the stack without removing it.

        Returns:
            Optional[T]: Either the item at the top of the stack, or None if it is empty.
        """
        if len(self) > 0:
            return self._list[-1]
        else:
            return None
