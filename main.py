"""Contains the main function for the program

This module should be used as the entry point for the whole program,
as it will then call the appropriate other entry points for the SRPN
functionality.

Example:
    Starting the program:

    $ python main.py
"""

import srpn


def main():
    """The main entry point for the program"""
    srpn.start_srpn()


if __name__ == "__main__":
    """Prevents anything being executed automatically if this module is imported"""
    main()
