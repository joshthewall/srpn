"""Contains functions related to parsing infix arithmetical input strings."""

from typing import Callable, List, Optional

from processing.calculator import operations
from processing.parsing import is_number, is_operator


def tokenize(line: str) -> List[str]:
    """Splits up the infix string into tokens depending on their type.

    Splits into either numbers, operators, or unknown characters.

    Args:
        line (str): The line to split up.

    Returns:
        List[str]: The resulting list of string tokens.

    Examples:
        >>> tokenize("123-43")
        ["123", "-", "43"]

        >>> tokenize("2^3*49+7-2/9")
        ['2', '^', '3', '*', '49', '+', '7', '-', '2', '/', '9']

        >>> tokenize("--1")  # This is an example to show that this follows SRPN's behaviour, rather than the "obvious"
        ['-', '-', '1']

        >>> tokenize("3--4")
        ['3', '-', '-4']
    """
    tokens = []
    current_number = []

    for current_char in line:
        if is_number(current_char):
            current_number.append(current_char)
            continue

        if current_char == '-' and len(current_number) == 0:
            # Next character is '-' and we aren't in the middle of a number -> could be start of a negative number,
            # so we append it as part of the current number, rather than treat it as a minus operator
            current_number.append(current_char)
            continue

        # The character is an operator or otherwise unknown, so we've reached the end of the current number
        if len(current_number) > 0:
            tokens.append(''.join(current_number))
            current_number = []

        # Operator or unknown character, so just append as is
        tokens.append(current_char)

    # Append any dangling numbers left over from the end
    if len(current_number) > 0:
        tokens.append(''.join(current_number))
        current_number = []

    return tokens


def evaluate_infix(tokens: List[str], process_input_fun: Callable[[str], Optional[str]]) -> List[str]:
    """Evaluates the infix expression given by the tokens, using the processor function passed.

    Based on the Shunting-yard algorithm, with operator precedences taken from the
    `operations` dictionary in the `calculator` module.

    Args:
        tokens (List[str]): A valid list of tokens, taken from the tokenization function.
        process_input_fun (Callable[[str], Optional[str]]): The processor function to use to evaluate the tokens

    Returns:
        List[str]: The list of result messages from processing the input.
    """
    result_msgs = []

    operators = []  # When this should be interpreted as a stack, it will be iterated on in reverse

    last_op_precedence = -1

    for token in tokens:
        if is_number(token) or token in ['r', '=']:
            # Immediately evaluate numbers, random number generation, or equals inputs
            result_msgs.append(process_input_fun(token))
        elif token == 'd':
            # Execute the current operators before printing the stack,
            # otherwise it would not be up to date
            for op in reversed(operators):
                result_msgs.append(process_input_fun(op))

            operators = []

            result_msgs.append(process_input_fun(token))
        elif is_operator(token):
            operation = operations[token]
            op_precedence = operation.precedence

            if op_precedence >= last_op_precedence:
                operators.append(token)
            else:
                # Current operator has lower precedence,
                # so operators already added should be evaluated first
                for op in reversed(operators):
                    result_msgs.append(process_input_fun(op))

                # Immediately add the operator to the operators list
                # after evaluating the higher precedence expression
                operators = [token]

            last_op_precedence = op_precedence
        else:
            result_msgs.append(f'Unrecognised operator or operand "{token}".')

    # Execute any dangling operators
    for dangling_op in reversed(operators):
        result_msgs.append(process_input_fun(dangling_op))

    operators = []

    return result_msgs
