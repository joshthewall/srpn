"""Contains functions related to parsing and differentiating user input."""

from typing import Optional

from processing.calculator import operations


def parse_number_from_start(input_str: str) -> Optional[str]:
    """Parses and returns a number at the beginning of a string.

    Determines whether there is a number using `is_number`,
    and empty strings don't contain any.

    Args:
        input_str (str): The string containing the number.

    Returns:
        Optional[str]: The number string, or None if a number cannot be found.
    """
    if len(input_str) == 0:
        return None

    if len(input_str) == 1:
        if is_number(input_str):
            return input_str
        else:
            return None

    digits_start_index = 0

    # Negative numbers -> digits start at index 1
    if input_str[0] == '-':
        if not is_number(input_str[1]):
            return None
        else:
            digits_start_index = 1

    # The number of characters from the start of the string we can go and still have a valid number
    max_valid_number_length = digits_start_index + 1

    # While we can go further an extra character and still have a valid number (and we haven't reached the end),
    # keep incrementing the maximum valid length.
    while is_number(input_str[:max_valid_number_length + 1]) and max_valid_number_length < len(input_str):
        max_valid_number_length += 1

    result = input_str[:max_valid_number_length]

    if result == "":
        return None

    return result


def is_digit(input_str: str) -> bool:
    """Determines whether the passed string only consists of ASCII base 10 digits.

    This is used instead of the Python built-in `isdigit` to avoid
    the inputting of unicode character that can't be converted with
    `int(...)`.

    Args:
        input_str (str): The string potentially consisting of digits.

    Returns:
        bool: True if the string only consists of digits; False if empty or contains non-digits.
    """
    if len(input_str) == 0:
        return False

    # ord('0') == 48
    zero_code_point = 48

    # ord('9') == 57
    nine_code_point = 57

    # True if all characters are within the '0' to '9' character range
    return all(ord(char) >= zero_code_point and ord(char) <= nine_code_point for char in input_str)


def is_number(input_str: str) -> bool:
    """Determines whether the passed string is a number.

    Format of a valid number:
        - Optional '-' prefix
        - Indefinite string of digits from the alphabet 0123456789

    Args:
        input_str (str): The string to check whether is a number or not.

    Returns:
        bool: True if it is a number; False if not or is empty.
    """
    if len(input_str) == 0:
        return False

    if len(input_str) == 1:
        return is_digit(input_str[0])

    # Negative number -> digits start at index 1
    digits_start_index = 1 if input_str[0] == '-' else 0

    # If not all characters, excluding the '-', are digits, then it is not a number
    if not all(is_digit(x) for x in input_str[digits_start_index:]):
        return False

    return True


def is_operator(input_str: str) -> bool:
    """Determines whether the passed string is a supported arithmetical operator.

    The supported operators are the keys in the `operations` dictionary within the 
    `calculator` module.

    Args:
        input_str (str): The string to check whether an operator or not.

    Returns:
        bool: True if the string is an operator; False if not.
    """
    return input_str in operations


def is_octal_prefixed(input_str: str) -> bool:
    """Determines whether the passed string is in the format of an octal number.

    A valid octal number format in this context is a number prefixed with '0' or '-0',
    with total length greater than 2.

    Args:
        input_str (str): The string to determine whether octal format or not.

    Returns:
        bool: True if it is representing an octal number format; False if not.
    """
    if len(input_str) <= 2:
        return False  # Too short to be an octal, by SRPN's behaviour

    if input_str[0] != '0' and not (input_str[0] == '-' and input_str[1] == '0'):
        return False  # Octals are prefixed with 0 or -0 if negative

    return True


def is_valid_octal(input_str: str) -> bool:
    """Checks that the number only contains valid octal digits with an optional '-' prefix.

    The valid octal digits are 0 to 7.

    Args:
        input_str (str): The string to check.

    Returns:
        bool: True if it contains only valid characters; False if it doesn't.
    """
    digits_start_index = 1 if input_str[0] == '-' else 0

    # ord('0') == 48
    zero_code_point = 48

    # ord('7') == 55
    seven_code_point = 55

    if not all(ord(char) >= zero_code_point and ord(char) <= seven_code_point for char in input_str[digits_start_index:]):
        return False  # Contains digits other than 0 to 7, so not octal, which is base 8

    return True
