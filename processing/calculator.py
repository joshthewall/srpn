"""Contains classes and functions for the SRPN calculator and related components."""

from inspect import signature
from typing import Callable, Optional

from data.stack import Stack, StackUnderflowError
from utility.c_calc import c_modulo, NegativePowerError, SIGNED_32BIT_INT_MIN, checked_power, limit_number


class BinaryOperation:
    """A binary operation for arithmetic on floats.

    Attributes:
        precedence: The precedence of the binary operator (smaller/more negative is lower).
        op_fun (Callable[[float, float], float]): The arithmetical function to execute on the inputs.
    """

    def __init__(self, precedence: int, op_fun: Callable[[float, float], float]):
        """Initialises the properties and checks op_fun is valid.

        Args:
            precedence (int): The precedence of the binary operator (smaller/more negative is lower).
            op_fun (Callable[[float, float], float]): The arithmetical function to execute on the inputs.

        Raises:
            ValueError: The passed function is not binary; i.e. doesn't have exactly 2 parameters.
        """
        if len(signature(op_fun).parameters) != 2:
            raise ValueError("The passed function doesn't have 2 parameters")

        self.precedence = precedence
        self.op_fun = op_fun


operations = {
    '-': BinaryOperation(0, lambda x, y: x - y),
    '+': BinaryOperation(1, lambda x, y: x + y),
    '*': BinaryOperation(2, lambda x, y: x * y),
    '/': BinaryOperation(3, lambda x, y: x / y),
    '%': BinaryOperation(4, lambda x, y: c_modulo(x, y)),
    '^': BinaryOperation(5, lambda x, y: checked_power(x, y))
}
"""The arithmetical operations available to the SRPN calculator."""


class SRPNCalculator():
    """An SRPN/stack-based calculator for performing arithmetic.

    Stack-based meaning that executing an operation will use the last two numbers 
    added to the calculator. For example:
    3 added
    2 added
    - executed
    will produce 3 - 2 = 1 as the calculator's current value.
    This is also known as Reverse Polish Notation (RPN).

    This calculator also follows SRPN (Saturated RPN), so the numbers are limited
    in the range -2^31 to 2^31 - 1, and will not exceed those, staying at the
    maximum/minimum values when attempting to.

    Stores floats internally but displays them as integers in `str`, truncated.

    Attributes:
        stack (Stack[float]): The arithmetic stack, used in popping numbers for operations and pushing the results onto.

    Examples:
        >>> calc = SRPNCalculator()
        >>> calc.add_number(3)
        >>> calc.add_number(5)
        >>> calc.execute_operation('*')
        >>> calc.get_current_value()
        15
    """

    def __init__(self):
        """Initialises the number stack."""
        self.stack = Stack[float](23)

    def __str__(self) -> str:
        # Display the int minimum if the stack is empty, to match SRPN behaviour
        if len(self.stack._list) == 0:
            return str(SIGNED_32BIT_INT_MIN)

        # e.g. _list = [3.5, 4.2, 1.9] -> truncated_nums = [3, 4, 1]
        truncated_nums = [str(int(x)) for x in self.stack._list]

        return '\n'.join(truncated_nums)

    def can_add_value(self) -> bool:
        """Checks whether the calculator is full or not.

        Returns:
            bool: True if a value can be inputted into the calculator; False if it is full.
        """
        return self.stack.can_push()

    def get_current_value(self) -> Optional[float]:
        """Gets the calculators current value; i.e the last number added or result of calculation.

        Returns:
            Optional[float]: Either the calculator's current value or None if nothing has been inputted.
        """
        if len(self.stack) == 0:
            return None

        return self.stack.peek()

    def add_number(self, num: int) -> bool:
        """Inputs an integer into the calculator.

        Will cap the number at the maximum or minimum value if it exceeds it,
        according to SRPN's limits.

        Args:
            num (int): The integer to input.

        Returns:
            bool: True if it was successful; False if the calculator is full.
        """
        return self.stack.push(limit_number(num))

    def execute_operation(self, operator: str):
        """Executes the arithmetical operation corresponding to the operator.

        Args:
            operator (str): The operator for the operation to execute.

        Raises:
            ValueError: The passed operator was not a valid operator.
            StackUnderflowError: There weren't enough numbers inputted into the calculator for the operation.
            Exceptions from arithmetical errors; e.g. ZeroDivisionError, NegativePowerError.
        """
        if operator not in operations:
            raise ValueError("The string passed was not a valid operator!")

        if len(self.stack) < 2:
            raise StackUnderflowError()

        operation = operations[operator]

        # Stack is LIFO, so second input is popped first
        input2 = self.stack.pop()
        input1 = self.stack.pop()

        # Already checked that the stack had the elements to pop, so can assert them
        assert input1 is not None and input2 is not None

        try:
            result = operation.op_fun(input1, input2)
            self.stack.push(limit_number(result))
        except (ZeroDivisionError, NegativePowerError):
            self.stack.push(input1)
            self.stack.push(input2)
            raise
