"""Contains the class and methods related to processing user input for SRPN."""
from typing import List, Optional

from data.stack import StackUnderflowError
from processing.calculator import SRPNCalculator
from processing.infix import evaluate_infix, tokenize
from processing.parsing import (is_number, is_octal_prefixed, is_operator,
                                is_valid_octal)
from utility.c_calc import NegativePowerError
from utility.c_random import RandomNumberGenerator
from utility.conversion import convert_octal_to_decimal


STACK_OVERFLOW_MESSAGE: str = "Stack overflow."
"""The message to display when something is attempted to be added to the stack but the stack is full."""


class InputProcessor:
    """Provides methods for processing input strings from the user and getting a response from SRPN functionality.

    Attributes:
        calculator (SRPNCalculator): The SRPN calculator to use for performing arithmetic.
        random_number_generator (RandomNumberGenerator): The random number generator to use for SRPN random number generation.
    """

    def __init__(self):
        """Initialises the calculator and random number generator."""
        self.calculator = SRPNCalculator()
        self.random_number_generator = RandomNumberGenerator()

    def process_input(self, input_str: str) -> Optional[str]:
        """Parses, then processes the input, returning an optional response.

        This is the method of choice for processing general input of unknown content.

        Args:
            input_str (str): The user input string to process.

        Returns:
            Optional[str]: The response produced from processing, or None if no response.
        """
        if (is_number(input_str)):
            # Starts with octal '0' or '-0' prefix but not neccesarily valid octal completely
            is_octal_like = is_octal_prefixed(input_str)

            # Contains only valid octal characters
            is_valid = is_valid_octal(input_str)

            if is_octal_like and not is_valid:
                # Check early for stack overflow even though no number would be added in order to match SRPN behaviour
                if self.calculator.can_add_value():
                    # Ignore numbers that are octal-prefixed but actually invalid octal
                    return None
                else:
                    return STACK_OVERFLOW_MESSAGE
            elif is_octal_like and is_valid:
                # Is a valid octal, so we can process it as a converted number from base 8 to 10
                return self.process_number(convert_octal_to_decimal(input_str))
            else:
                # Is just a base-10 number
                return self.process_number(int(input_str))

        if (input_str == 'r'):  # Produce a random number
            # Check beforehand, so that the generator doesn't change state if it can't add to the calculator
            if self.calculator.can_add_value():
                return self.process_number(self.random_number_generator.generate())
            else:
                return STACK_OVERFLOW_MESSAGE  # The calculator is full

        if (input_str == 'd'):  # Display the current state of the calculator
            return str(self.calculator)

        if (input_str == '='):  # Display the top value of the calculator stack
            currentValue = self.calculator.get_current_value()
            return "Stack empty." if currentValue is None else str(int(currentValue))  # Truncated

        if (is_operator(input_str)):
            try:
                self.calculator.execute_operation(input_str)
                return None
            except StackUnderflowError:
                return "Stack underflow."
            except ZeroDivisionError:
                return "Divide by 0."
            except NegativePowerError:
                return "Negative power."

        # If it's not any of the above, just interpret it as infix/as a joined line
        processed_result = self.process_joined_line(input_str)

        if processed_result:
            error_msgs = [msg for msg in processed_result if msg is not None]
            return '\n'.join(error_msgs) if error_msgs else None

        return None

    def process_joined_line(self, line: str) -> Optional[List[str]]:
        """Processes a line of characters not containing whitespace, returning an optional list of responses.

        Args:
            line (str): The non-whitespace containing line to parse and process.

        Returns:
            Optional[str]: A potential result message or None.
        """
        tokens = tokenize(line)
        result = evaluate_infix(tokens, self.process_input)

        return result

    def process_number(self, num: int) -> Optional[str]:
        """Processes a number, potentially adding it to the calculator.

        If the calculator stack isn't full, the number is added, otherwise
        an overflow message is returned.

        Args:
            num (int): The number to input into the calculator.

        Returns:
            Optional[str]: None if it was successful; otherwise, a stack overflow error message.
        """
        added = self.calculator.add_number(num)

        return None if added else STACK_OVERFLOW_MESSAGE
