"""Contains the main function and related constants for SRPN.

This module should be used to start the SRPN functionality
from another module.

Example:
    import srpn

    srpn.start_srpn()
"""
from utility.c_calc import CrashProgramError
from processing.processor import InputProcessor


COMMENT_CHAR: str = '#'
"""The character to recognise as a comment start/end."""


def start_srpn():
    """Starts the main input loop for SRPN.

    Indefinitely takes in lines of input, processing them and performing actions
    according to the SRPN functionality.

    Breaks the loop on:
        EOFError: An EOF is entered by the user or standard input reaches its end.
        KeyboardInterrupt: A keyboard interrupt signal is received.
        CrashProgramError: A general "crash" has occurred; such as unhandled floating point exceptions in the original SRPN program.
    """
    processor = InputProcessor()

    in_comment = False

    while True:
        try:
            line = input()
        except (EOFError, KeyboardInterrupt):
            break  # Should exit cleanly under these conditions, as per SRPN behaviour

        if line.isspace():  # Ignore whitespace
            continue

        words = line.split()  # Extra whitespace is handled (ignored) in split

        for word in words:
            if word == COMMENT_CHAR:
                in_comment = not in_comment
                continue

            if in_comment:
                pass  # Ignore all non-comment-character words in comments
            else:
                try:
                    response = processor.process_input(word)

                    if response is not None:
                        print(response)
                except CrashProgramError:
                    # Just exit to simulate a crash
                    return
