# SRPN

A reverse Polish notation/stack-based calculator, allowing for integer arithmetic in postfix and infix notation, using octal or decimal input, or random number generation.

## About

This was created for a coursework that required the replication of a program called SRPN (including any quirks/bugs), given just the executable file. The methods I used included inputting test data and comparing, as well as using the [Snowman](https://github.com/yegord/snowman) decompiler to help manually map out parts of the source code for more intricate behaviour.


## Usage

Run the program by doing the following on the command line, 
with the working directory at the top level of the project:

`python main.py`

This will start a read loop, taking in lines of input and processing them to perform some operation within the calculator. <br/> Input is separated by a newline or space and can be any of the following:
- an integer (decimal, or octal with a `0` prefix)
- an operator (`-`, `+`, `*`, `/`, `%`, `^`)
- a postfix expression; e.g. `3 5 +`
- an infix expression (no whitespace); e.g. `7*23`
- `r` to push a randomly generated number onto the stack
- `d` to display the calculator's stack
- `=` to display the top value on the stack
- a comment surrounded in `#`; e.g. `7 3 * # multiplies 7 by 3 #`
