"""Contains the generator class for creating random numbers according to SRPN's behaviour"""

import itertools
from typing import Iterator

from utility.c_calc import SIGNED_32BIT_INT_MAX, UNSIGNED_32BIT_INT_MAX_MOD


class RandomNumberGenerator:
    """A random number generator that behaves how SRPN's does.

    Generates according to the GLIBC random() algorithm.
    It resets once after the 22nd generation of a number, and then
    continues indefinitely according to the algorithm.

    Attributes:
        generator (Iterator[Int]): The current instance of the random number generator iterator.
    """

    def __init__(self):
        self._generated = []
        self._generated_count = 0
        self.generator = self._glibc_random_generator()

    @property
    def seed(self) -> int:
        """The seed for the generator; i.e. the number to start calculating with.

        Will always be 1 for the SRPN behaviour.
        """
        return 1

    def generate(self) -> int:
        """Generates and returns a "random" number.

        "random" meaning that it resets once after the 22nd generation
        but otherwise generates random numbers after that.

        Returns:
            int: The newly generated random number.
        """
        if self._generated_count == 22:
            self.reset_generator()

        self._generated_count += 1

        return next(self.generator)

    def reset_generator(self):
        """Resets the random number generator, keeping the same seed and generation count."""
        self._generated = []
        self.generator = self._glibc_random_generator()

    def _glibc_random_generator(self) -> Iterator[int]:
        """Generates random numbers according to the GLIBC random() algorithm

        Based on description at: https://www.mscs.dal.ca/~selinger/random/
        """
        randomNums = self._generated

        randomNums.append(self.seed)
        for i in range(1, 31):
            randomNums.append((16807 * randomNums[i - 1]) % SIGNED_32BIT_INT_MAX)

        for i in range(31, 34):
            randomNums.append(randomNums[i - 31])

        for i in range(34, 344):
            randomNums.append((randomNums[i - 31] + randomNums[i - 3]) % UNSIGNED_32BIT_INT_MAX_MOD)

        for i in itertools.count(344):
            randomNums.append((randomNums[i - 31] + randomNums[i - 3]) % UNSIGNED_32BIT_INT_MAX_MOD)
            yield (randomNums[i] >> 1)
