"""Contains utility constants and functions, mainly relevant to re-implementing C arithmetic behaviour"""

SIGNED_32BIT_INT_MAX = 2147483647
SIGNED_32BIT_INT_MIN = -2147483648

UNSIGNED_32BIT_INT_MAX = 4294967295

SIGNED_64BIT_INT_MAX = 9223372036854775807
SIGNED_64BIT_INT_MIN = -9223372036854775808

SIGNED_32BIT_INT_MAX_MOD = 2147483648
UNSIGNED_32BIT_INT_MAX_MOD = 4294967296
"""MOD suffixed constants are for use as the modulus in overflow calculation or related operations"""


def limit_number(num: float) -> float:
    """Returns the limited number according to the maximum and minimum of SRPN's calculator.

    Follows C limits for the `int` data type:
    - Maximum is signed 32 bit integer max (2^31 - 1)
    - Minimum is signed 32 bit integer min (-2^31)

    Args:
        num (float): The number to limit.

    Returns:
        float: The limited number, unchanged if it was already within the limits.
    """
    limited_number = num

    if limited_number > SIGNED_32BIT_INT_MAX:
        limited_number = SIGNED_32BIT_INT_MAX
    elif limited_number < SIGNED_32BIT_INT_MIN:
        limited_number = SIGNED_32BIT_INT_MIN

    return limited_number


class CrashProgramError(Exception):
    """An exception to signify that the SRPN program should "crash" 

    To be used for replicating SRPN behaviour of crashing at certain inputs, such
    as when an unhandled floating point exception occurs.
    """
    pass


def c_modulo(x: float, y: float) -> float:
    """Performs the modulo operation with behaviour as in C99

    Args:
        x (float): The dividend.
        y (float): The divisor.

    Returns:
        float: The remainder of dividing x by y.
    """
    # Not mathematically divison by zero, but SRPN says it is, so we follow it
    if x == 0:
        raise ZeroDivisionError()

    if y == 0:
        raise CrashProgramError()

    if (x < 0):
        return -c_modulo(-x, y)

    if (y < 0):
        return c_modulo(x, -y)

    return x % y


class NegativePowerError(Exception):
    """When an exponent less than zero has been attempted to be used on a power operation"""
    pass


def checked_power(x: float, y: float) -> float:
    """A power function that prevents negative exponents.

    Performs `x` to the power of `y`, if `y` is not negative

    Args:
        x (float): The base for the exponentiation.
        y (float): The exponent for the exponentiation.

    Raises:
        NegativePowerError: The exponenet passed was negative.

    Returns:
        float: The result of the exponentiation,
               or `SIGNED_32BIT_INT_MIN` if attempting to raise a negative number 
               to a fractional power, to match SRPN behaviour.
    """
    if y < 0:
        raise NegativePowerError()

    if x < 0 and (y > 0 and y < 1):
        return SIGNED_32BIT_INT_MIN

    return x ** y
