"""Contains functions related to converting between different types of numbers"""

from utility.c_calc import (SIGNED_32BIT_INT_MAX, SIGNED_32BIT_INT_MAX_MOD,
                            SIGNED_64BIT_INT_MAX, SIGNED_64BIT_INT_MIN,
                            UNSIGNED_32BIT_INT_MAX_MOD)


def convert_octal_to_decimal(octal_num_str: str) -> int:
    """Converts an octal number string to a decimal int.

    Deliberately uses a faulty conversion in that there is no validation
    of the digits being within range, in order to match SRPN behaviour.

    It also bounds the range of the input as a decimal number in order to 
    match SRPN behaviour, which uses C `atoi` before passing to their
    conversion funciton.

    Args:
        octal_num_str (str): The representation of the octal number as a string .

    Returns:
        int: The resulting decimal value from conversion.
    """
    is_negative = octal_num_str[0] == '-'

    octal_num_decimal_val = int(octal_num_str)

    if is_negative:
        if octal_num_decimal_val < SIGNED_64BIT_INT_MIN:
            # Following SRPN behaviour for 64 bit overflow in negative direction
            return 0
        else:
            # Same as the positive's result but negated
            return -convert_octal_to_decimal(octal_num_str[1:])

    if octal_num_decimal_val > SIGNED_64BIT_INT_MAX:
        # Following SRPN behaviour for 64 bit overflow
        return -1

    if octal_num_decimal_val > SIGNED_32BIT_INT_MAX:
        # Convert 32 bit overflowed integer back to signed 32 bit int range
        octal_num_decimal_val = ((octal_num_decimal_val + SIGNED_32BIT_INT_MAX_MOD) %
                                 UNSIGNED_32BIT_INT_MAX_MOD) - SIGNED_32BIT_INT_MAX_MOD

    octal_str = str(octal_num_decimal_val)

    # Negative -> digits start at index 1
    start_index = 1 if octal_str[0] == '-' else 0
    decimal_result_is_negative = start_index == 1

    decimal_val = 0

    # Sum the digits multiplied by the corresponding power of 8
    # e.g. 123 -> 1 * 8^2 + 2 * 8^1 + 3 * 8^0 = 83
    for i in range(0, len(octal_str[start_index:])):
        decimal_val += int(octal_str[len(octal_str) - 1 - i]) * (8 ** i)

    return -decimal_val if decimal_result_is_negative else decimal_val
